# Author - Socian Ltd.
# https://socian.ai

import os
import pickle

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from tqdm import tqdm
import pandas as pd


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret.
# Please change the Client Secret File.
CLIENT_SECRETS_FILE = "client_secret.json" #This is the name of your JSON file

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'


# Method for Authentication. As every time, the App needs authentication, saving the value in Pickle to use later.
def get_authenticated_service():
    # Loading the Data using Pickle if file exists
    if os.path.exists("credentials_pickle.txt"):
        with open("credentials_pickle.txt", 'rb') as f:
            credentials = pickle.load(f)
    else:
        # Saving the data using pickle so that it can be used later on and authentication is not required every time
        flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
        credentials = flow.run_console()
        with open("credentials_pickle.txt", 'wb') as f:
            pickle.dump(credentials, f)

    # Building the API to be used later on
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)

# Authenticating using OAUTH
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
service = get_authenticated_service()
print("Authenticated")


# =============================================================================
# Get Top Query Results - Search Query Initialisation
# =============================================================================

# Loading List of Queries from CSV
ytb_queries = pd.read_csv('H:\SOCIAN\PROJECTS_v4\AI_v4\YouTubeCrawler\ytb_queries.csv')
queries = ytb_queries["queries"]

for i in range(0, len(queries)):
    query = queries[i]
    # Searching in YouTube and Loading the Top Search Results
    query_results = service.search().list(
            part = 'snippet',
            q = query,
            order = 'relevance', # You can consider using viewCount
            maxResults = 50,
            type = 'video', # Channels might appear in search results
            relevanceLanguage = 'bn',
            safeSearch = 'moderate',
            ).execute()

    # =============================================================================
    # Get Top Videos - Get Video IDs
    # =============================================================================
    video_id = []
    channel = []
    video_title = []
    video_desc = []
    for item in query_results['items']:
        # For Every relevant Searched Videos, Loading the Videoid, Title, Channel & Description
        video_id.append(item['id']['videoId'])
        channel.append(item['snippet']['channelTitle'])
        video_title.append(item['snippet']['title'])
        video_desc.append(item['snippet']['description'])

    # =============================================================================
    # Get Comments of Top Videos
    # =============================================================================

    # Adding Headers to add on Output File
    query_pop = ['Query']
    video_id_pop = ['Video ID']
    channel_pop = ['Channel Name']
    video_title_pop = ['Video Title']
    video_desc_pop = ['Video Description']
    comments_pop = ['Comment']
    comment_id_pop = ['Comment ID']
    reply_count_pop = ['Comment Reply']
    like_count_pop = ['Comment Like']

    for i, video in enumerate(tqdm(video_id, ncols=100)):
        try:
            response = service.commentThreads().list(
                part='snippet',
                videoId=video,
                maxResults=100,  # Only take top 100 comments...
                order='relevance',  # ... ranked on relevance
                textFormat='plainText',
            ).execute()
        except:
            # Some Videos have comments disabled
            print("Exception. Comment is Disabled")

        comments_temp = []
        comment_id_temp = []
        reply_count_temp = []
        like_count_temp = []
        # Putting the Data in List so that they can be used later on
        for item in response['items']:
            comments_temp.append(item['snippet']['topLevelComment']['snippet']['textDisplay'])
            comment_id_temp.append(item['snippet']['topLevelComment']['id'])
            reply_count_temp.append(item['snippet']['totalReplyCount'])
            like_count_temp.append(item['snippet']['topLevelComment']['snippet']['likeCount'])
        comments_pop.extend(comments_temp)
        comment_id_pop.extend(comment_id_temp)
        reply_count_pop.extend(reply_count_temp)
        like_count_pop.extend(like_count_temp)

        video_id_pop.extend([video_id[i]] * len(comments_temp))
        channel_pop.extend([channel[i]] * len(comments_temp))
        video_title_pop.extend([video_title[i]] * len(comments_temp))
        video_desc_pop.extend([video_desc[i]] * len(comments_temp))

    query_pop.extend([query] * (len(video_id_pop)-1))



    # =============================================================================
    # Populate to Dataframe
    # =============================================================================
    output_dict = {
            'Query': query_pop,
            'Channel': channel_pop,
            'Video Title': video_title_pop,
            'Video Description': video_desc_pop,
            'Video ID': video_id_pop,
            'Comment': comments_pop,
            'Comment ID': comment_id_pop,
            'Replies': reply_count_pop,
            'Likes': like_count_pop,
            }

    # Writing the Result in CSV
    output_df = pd.DataFrame(output_dict, columns = output_dict.keys())
    output_df.to_csv("bn_comment_" + query + ".csv", header=None, index=False)
